<?php

namespace App\Entity;

use App\Repository\MemeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MemeRepository::class)
 */
class Meme
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="favMemes")
     */
    private $userMeme;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->userMeme = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUserMeme(): Collection
    {
        return $this->userMeme;
    }

    public function addUserMeme(User $userMeme): self
    {
        if (!$this->userMeme->contains($userMeme)) {
            $this->userMeme[] = $userMeme;
            $userMeme->addFavMeme($this);
        }

        return $this;
    }

    public function removeUserMeme(User $userMeme): self
    {
        if ($this->userMeme->removeElement($userMeme)) {
            $userMeme->removeFavMeme($this);
        }

        return $this;
    }
}
