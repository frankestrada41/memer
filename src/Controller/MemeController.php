<?php

namespace App\Controller;


use App\Entity\Meme;
use Cloudinary\Api\Upload\UploadApi;

use Cloudinary\Configuration\Configuration;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\User;

Configuration::instance([
  'cloud' => [
    'cloud_name' => 'dbgytuucv',
    'api_key' => '496977518617423',
    'api_secret' => '7Chzqy0FajTpiMSy-JSKtUzQhI8'
  ],
  'url' => [
    'secure' => true
  ]
]);


class MemeController extends AbstractController
{
  /**
   * @Route("/" , name="home")
   */
  public function home()
  {
    return $this->render('home.html.twig');
  }

  /**
   * @Route("/form" , name="form")
   */
  public function form(EntityManagerInterface $em, Request $req)
  {
    $name = $req->request->get('name');
    $text = $req->request->get('text');
    $image = $req->request->get('image');



    // if ($image) {
    //   $uploadimage = (new UploadApi())->upload($image);
    // }


    if ($name) {
      $newMeme = new Meme;
      $newMeme->setName($name);
      $newMeme->setImage($image);
      $newMeme->setText($text);

      $em->persist($newMeme);
      $em->flush();
    }

    return $this->render('form.html.twig');
  }
  /**
   * @Route("/allmemes" , name="allmemes")
   */
  public function allMemes(EntityManagerInterface $em)
  {
    $repo = $em->getRepository(Meme::class);
    $allMemes = $repo->findAll();

    return $this->render('allMemes.html.twig', ["allMemes" => $allMemes]);
  }
  /**
   * @Route("/mymemes" , name="mymemes")
   */
  public function myMemes(EntityManagerInterface $doctrine)
  {
    $user = $this->getUser();
    $userMemes = $user->getFavMemes();

    return $this->render('myMemes.html.twig', ['favMemes' => $userMemes]);
  }

  /**
   * @Route("/addmeme/{id}" , name="addmeme")
   */
  public function addMeme(EntityManagerInterface $doctrine, $id)
  {
    $repo = $doctrine->getRepository(Meme::class);
    $favMeme = $repo->find($id);
    $userRepo = $this->getUser();
    $userRepo->addFavMeme($favMeme);
    $doctrine->flush();

    return $this->redirectToRoute('mymemes');
  }
}
