<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210722185001 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_meme');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_meme (user_id INT NOT NULL, meme_id INT NOT NULL, INDEX IDX_311E35FDA76ED395 (user_id), INDEX IDX_311E35FDDB6EC45D (meme_id), PRIMARY KEY(user_id, meme_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_meme ADD CONSTRAINT FK_311E35FDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_meme ADD CONSTRAINT FK_311E35FDDB6EC45D FOREIGN KEY (meme_id) REFERENCES meme (id) ON UPDATE NO ACTION ON DELETE CASCADE');
    }
}
